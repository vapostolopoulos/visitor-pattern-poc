package eu.ubitech.dto;

public class KubernetesProjectDto extends ProjectDto {
    private String somethingK8sRelated;

    public KubernetesProjectDto() {}

    public KubernetesProjectDto(String id, String name, String somethingK8sRelated) {
        super(id, name);
        this.somethingK8sRelated = somethingK8sRelated;
    }

    public String getSomethingK8sRelated() {
        return somethingK8sRelated;
    }

    public void setSomethingK8sRelated(String somethingK8sRelated) {
        this.somethingK8sRelated = somethingK8sRelated;
    }
}
