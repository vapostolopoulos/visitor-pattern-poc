package eu.ubitech.dto;

public class OpenStackProjectDto extends ProjectDto {
    private String networkId;
    private String externalNetworkId;

    public OpenStackProjectDto() {}

    public OpenStackProjectDto(String id, String name, String networkId, String externalNetworkId) {
        super(id, name);
        this.networkId = networkId;
        this.externalNetworkId = externalNetworkId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getExternalNetworkId() {
        return externalNetworkId;
    }

    public void setExternalNetworkId(String externalNetworkId) {
        this.externalNetworkId = externalNetworkId;
    }
}
