package eu.ubitech.dto;

public class SliceDto {
    private String id;
    private ProjectDto project;

    public SliceDto() {}

    public SliceDto(String id, ProjectDto project) {
        this.id = id;
        this.project = project;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProjectDto getProject() {
        return project;
    }

    public void setProject(ProjectDto project) {
        this.project = project;
    }
}
