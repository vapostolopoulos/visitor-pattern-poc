package eu.ubitech.visitor;

import eu.ubitech.dto.KubernetesProjectDto;
import eu.ubitech.dto.OpenStackProjectDto;
import eu.ubitech.dto.ProjectDto;
import eu.ubitech.dto.SliceDto;
import eu.ubitech.model.KubernetesProject;
import eu.ubitech.model.OpenStackProject;
import eu.ubitech.model.Project;
import eu.ubitech.model.Slice;

public class DtoConverterVisitor implements Visitor {
    @Override
    public SliceDto visit(Slice slice) {
        System.out.println("Inside SliceVisit");

        final var project = slice.getProject();
        final var projectDto = project.accept(this);
        return new SliceDto(slice.getId(), projectDto);
    }

    @Override
    public ProjectDto visit(Project project) {
        System.out.println("Inside ProjectVisit");

        return new ProjectDto(project.getId(), project.getName());
    }

    @Override
    public OpenStackProjectDto visit(OpenStackProject openStackProject) {
        System.out.println("Inside OpenStackProjectVisit");

        return new OpenStackProjectDto(
                openStackProject.getId(),
                openStackProject.getName(),
                openStackProject.getNetworkId(),
                openStackProject.getExternalNetworkId());
    }

    @Override
    public KubernetesProjectDto visit(KubernetesProject kubernetesProject) {
        System.out.println("Inside KubernetesProjectVisit");

        return new KubernetesProjectDto(
                kubernetesProject.getId(),
                kubernetesProject.getName(),
                kubernetesProject.getSomethingK8sRelated());
    }
}
