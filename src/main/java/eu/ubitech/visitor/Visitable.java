package eu.ubitech.visitor;

public interface Visitable<T> {
    T accept(Visitor visitor);
}
