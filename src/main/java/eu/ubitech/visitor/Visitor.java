package eu.ubitech.visitor;

import eu.ubitech.dto.KubernetesProjectDto;
import eu.ubitech.dto.OpenStackProjectDto;
import eu.ubitech.dto.ProjectDto;
import eu.ubitech.dto.SliceDto;
import eu.ubitech.model.KubernetesProject;
import eu.ubitech.model.OpenStackProject;
import eu.ubitech.model.Project;
import eu.ubitech.model.Slice;

public interface Visitor {
    SliceDto visit(Slice slice);

    ProjectDto visit(Project project);

    OpenStackProjectDto visit(OpenStackProject openStackProject);

    KubernetesProjectDto visit(KubernetesProject kubernetesProject);
}
