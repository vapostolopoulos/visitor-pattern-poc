package eu.ubitech.model;

import eu.ubitech.dto.OpenStackProjectDto;
import eu.ubitech.visitor.Visitor;

public class OpenStackProject extends Project {
    private final String networkId;
    private final String externalNetworkId;

    public OpenStackProject(String id, String name, String networkId, String externalNetworkId) {
        super(id, name);
        this.networkId = networkId;
        this.externalNetworkId = externalNetworkId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public String getExternalNetworkId() {
        return externalNetworkId;
    }

    @Override
    public OpenStackProjectDto accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
