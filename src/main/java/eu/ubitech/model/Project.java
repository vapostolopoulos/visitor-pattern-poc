package eu.ubitech.model;

import eu.ubitech.dto.ProjectDto;
import eu.ubitech.visitor.Visitable;
import eu.ubitech.visitor.Visitor;

public class Project implements Visitable<ProjectDto> {
    private final String id;
    private final String name;

    public Project(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProjectDto accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
