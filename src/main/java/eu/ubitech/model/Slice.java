package eu.ubitech.model;

import eu.ubitech.dto.SliceDto;
import eu.ubitech.visitor.Visitable;
import eu.ubitech.visitor.Visitor;

public class Slice implements Visitable<SliceDto> {
    private final String id;
    private final Project project;

    public Slice(String id, Project project) {
        this.id = id;
        this.project = project;
    }

    public String getId() {
        return id;
    }

    public Project getProject() {
        return project;
    }

    public SliceDto accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
