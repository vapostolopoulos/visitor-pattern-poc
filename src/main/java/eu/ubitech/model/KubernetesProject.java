package eu.ubitech.model;

import eu.ubitech.dto.KubernetesProjectDto;
import eu.ubitech.visitor.Visitor;

public class KubernetesProject extends Project {
    private final String somethingK8sRelated;

    public KubernetesProject(String id, String name, String somethingK8sRelated) {
        super(id, name);
        this.somethingK8sRelated = somethingK8sRelated;
    }

    public String getSomethingK8sRelated() {
        return somethingK8sRelated;
    }

    @Override
    public KubernetesProjectDto accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
