package eu.ubitech;

import static org.assertj.core.api.Assertions.assertThat;

import eu.ubitech.dto.KubernetesProjectDto;
import eu.ubitech.dto.OpenStackProjectDto;
import eu.ubitech.dto.ProjectDto;
import eu.ubitech.dto.SliceDto;
import eu.ubitech.model.KubernetesProject;
import eu.ubitech.model.OpenStackProject;
import eu.ubitech.model.Project;
import eu.ubitech.model.Slice;
import eu.ubitech.visitor.DtoConverterVisitor;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

@QuarkusTest
class DoubleDispatchTest {

    @Test
    void shouldConvertSliceWithOpenstackDomainHierarchyToDtoHierarchy() {
        final var expectedId = "Id";
        final var expectedName = "Name";
        final var expectedNetworkId = "NetworkId";
        final var expectedExternalNetworkId = "ExternalNetworkId";
        final var expectedOpenStackProjectDto =
                new OpenStackProjectDto(
                        expectedId, expectedName, expectedNetworkId, expectedExternalNetworkId);
        final var sliceId = "SliceId";
        final var expectedSliceDto = new SliceDto(sliceId, expectedOpenStackProjectDto);

        final var openStackProject =
                new OpenStackProject(
                        expectedId, expectedName, expectedNetworkId, expectedExternalNetworkId);
        final var slice = new Slice(sliceId, openStackProject);

        final var dtoConverterVisitor = new DtoConverterVisitor();
        final var receivedSliceDto = slice.accept(dtoConverterVisitor);

        assertThat(receivedSliceDto).usingRecursiveComparison().isEqualTo(expectedSliceDto);
    }

    @Test
    void shouldConvertSliceWithKubernetesDomainHierarchyToDtoHierarchy() {
        final var expectedId = "Id";
        final var expectedName = "Name";
        final var expectedSomethingK8sRelated = "SomethingK8sRelated";
        final var expectedKubernetesProjectDto =
                new KubernetesProjectDto(expectedId, expectedName, expectedSomethingK8sRelated);
        final var sliceId = "SliceId";
        final var expectedSliceDto = new SliceDto(sliceId, expectedKubernetesProjectDto);

        final var kubernetesProject =
                new KubernetesProject(expectedId, expectedName, expectedSomethingK8sRelated);
        final var slice = new Slice(sliceId, kubernetesProject);

        final var dtoConverterVisitor = new DtoConverterVisitor();

        final var receivedSliceDto = slice.accept(dtoConverterVisitor);

        assertThat(receivedSliceDto).usingRecursiveComparison().isEqualTo(expectedSliceDto);
    }

    @Test
    void shouldConvertSliceWithProjectDomainHierarchyToDtoHierarchy() {
        final var expectedId = "Id";
        final var expectedName = "Name";
        final var projectDto = new ProjectDto(expectedId, expectedName);
        final var sliceId = "SliceId";
        final var expectedSliceDto = new SliceDto(sliceId, projectDto);

        final var project = new Project(expectedId, expectedName);
        final var slice = new Slice(sliceId, project);

        final var dtoConverterVisitor = new DtoConverterVisitor();

        final var receivedSliceDto = slice.accept(dtoConverterVisitor);

        assertThat(receivedSliceDto).usingRecursiveComparison().isEqualTo(expectedSliceDto);
    }
}
